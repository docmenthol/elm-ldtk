module LDTK exposing (Ldtk, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP
import LDTK.Definitions as Definitions exposing (Definitions)
import LDTK.Layout as Layout exposing (Layout)
import LDTK.Level as Level exposing (Level)


type alias Ldtk =
    { bgColor : String
    , defs : Definitions
    , jsonVersion : String
    , levels : List Level
    , worldGridHeight : Int
    , worldGridWidth : Int
    , worldLayout : Layout
    }


decoder : D.Decoder Ldtk
decoder =
    D.succeed Ldtk
        |> DP.required "bgColor" D.string
        |> DP.required "defs" Definitions.decoder
        |> DP.required "jsonVersion" D.string
        |> DP.required "levels" (D.list Level.decoder)
        |> DP.required "worldGridHeight" D.int
        |> DP.required "worldGridWidth" D.int
        |> DP.required "worldLayout" Layout.decoder
