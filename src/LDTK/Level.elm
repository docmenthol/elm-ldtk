module LDTK.Level exposing (Level, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP


type Level
    = Level


decoder : D.Decoder Level
decoder =
    D.succeed Level
