module LDTK.Tileset exposing (Tileset, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP


type alias Tileset =
    { cHei : Int
    , cWid : Int
    , customData : List CustomData
    , enumTags : List EnumTag
    , identifier : String
    , padding : Int
    , pxHei : Int
    , pxWid : Int
    , relPath : String
    , spacing : Int
    , tagsSourceEnumUid : Maybe Int
    , tileGridSize : Int
    , uid : Int
    }


type alias CustomData =
    { data : String
    , tileId : Int
    }


type alias EnumTag =
    { enumValueId : String
    , tileIds : List Int
    }


decoder : D.Decoder Tileset
decoder =
    D.succeed Tileset
        |> DP.required "__cHei" D.int
        |> DP.required "__cWid" D.int
        |> DP.required "customData" (D.list customDataDecoder)
        |> DP.required "enumTags" (D.list enumTagDecoder)
        |> DP.required "identifier" D.string
        |> DP.required "padding" D.int
        |> DP.required "pxHei" D.int
        |> DP.required "pxWid" D.int
        |> DP.required "relPath" D.string
        |> DP.required "spacing" D.int
        |> DP.required "tagsSourceEnumUid" (D.nullable D.int)
        |> DP.required "tileGridSize" D.int
        |> DP.required "uid" D.int


customDataDecoder : D.Decoder CustomData
customDataDecoder =
    D.succeed CustomData
        |> DP.required "data" D.string
        |> DP.required "tileId" D.int


enumTagDecoder : D.Decoder EnumTag
enumTagDecoder =
    D.succeed EnumTag
        |> DP.required "enumValueId" D.string
        |> DP.required "tileIds" (D.list D.int)
