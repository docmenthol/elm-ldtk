module LDTK.LayerInstance exposing (LayerInstance, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP
import LDTK.EntityInstance as EntityInstance exposing (EntityInstance)
import LDTK.TileInstance as TileInstance exposing (TileInstance)


type alias LayerInstance =
    { cHei : Int
    , cWid : Int
    , gridSize : Int
    , identifier : String
    , opacity : Float
    , pxTotalOffsetX : Int
    , pxTotalOffsetY : Int
    , tilesetDefUid : Maybe Int
    , tilesetRelPath : Maybe String
    , layerType : LayerType
    , autoLayerTiles : List TileInstance
    , entityInstances : List EntityInstance
    , gridTiles : List TileInstance
    , intGridCsv : List Int
    , layerDefUid : Int
    , levelId : Int
    , overrideTilesetUid : Maybe Int
    , pxOffsetX : Int
    , pxOffsetY : Int
    , visible : Bool
    }


type LayerType
    = IntGrid
    | Entities
    | Tiles
    | AutoLayer


decoder : D.Decoder LayerInstance
decoder =
    D.succeed LayerInstance
        |> DP.required "__cHei" D.int
        |> DP.required "__cWid" D.int
        |> DP.required "__gridSize" D.int
        |> DP.required "__identifier" D.string
        |> DP.required "__opacity" D.float
        |> DP.required "__pxTotalOffsetX" D.int
        |> DP.required "__pxTotalOffsetY" D.int
        |> DP.required "__tilesetDefUid" (D.nullable D.int)
        |> DP.required "__tilesetRelPath" (D.nullable D.string)
        |> DP.required "__type" layerTypeDecoder
        |> DP.required "autoLayerTiles" (D.list TileInstance.decoder)
        |> DP.required "entityInstances" (D.list EntityInstance.decoder)
        |> DP.required "gridTiles" (D.list TileInstance.decoder)
        |> DP.required "intGridCsv" (D.list D.int)
        |> DP.required "layerDefUid" D.int
        |> DP.required "levelId" D.int
        |> DP.required "overrideTilesetUid" (D.nullable D.int)
        |> DP.required "pxOffsetX" D.int
        |> DP.required "pxOffsetY" D.int
        |> DP.required "visible" D.bool


layerTypeDecoder : D.Decoder LayerType
layerTypeDecoder =
    let
        toType lt =
            case lt of
                "Entities" ->
                    D.succeed Entities

                "Tiles" ->
                    D.succeed Tiles

                "AutoLayer" ->
                    D.succeed AutoLayer

                _ ->
                    D.succeed IntGrid
    in
    D.string |> D.andThen toType
