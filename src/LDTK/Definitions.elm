module LDTK.Definitions exposing (Definitions, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP
import LDTK.Entity as Entity exposing (Entity)
import LDTK.Enum as Enum exposing (Enum)
import LDTK.Layer as Layer exposing (Layer)
import LDTK.Tileset as Tileset exposing (Tileset)


type alias Definitions =
    { entities : List Entity
    , enums : List Enum
    , externalEnums : List Enum
    , layers : List Layer
    , tilesets : List Tileset
    }


decoder : D.Decoder Definitions
decoder =
    D.succeed Definitions
        |> DP.required "entities" (D.list Entity.decoder)
        |> DP.required "enums" (D.list Enum.decoder)
        |> DP.required "externalEnums" (D.list Enum.decoder)
        |> DP.required "layers" (D.list Layer.decoder)
        |> DP.required "tilesets" (D.list Tileset.decoder)
