module LDTK.FieldInstance exposing (FieldInstance, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP


type alias FieldInstance =
    { identifier : String
    , fieldType : String

    --, value : a
    , defUid : Int
    }


decoder : D.Decoder FieldInstance
decoder =
    D.succeed FieldInstance
        |> DP.required "__identifier" D.string
        |> DP.required "__type" D.string
        --|> DP.required "__value" (D.map Result.toMaybe D.decodeValue)
        |> DP.required "defUid" D.int
