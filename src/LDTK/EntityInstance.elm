module LDTK.EntityInstance exposing (EntityInstance, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP
import LDTK.FieldInstance as FieldInstance exposing (FieldInstance)


type alias EntityInstance =
    { grid : List Int
    , identifier : String
    , pivot : List Float
    , tile : Maybe Tile
    , defUid : Int
    , fieldInstances : List FieldInstance
    , height : Int
    , px : List Int
    , width : Int
    }


type alias Tile =
    { srcRect : List Int
    , tilesetUid : Int
    }


decoder : D.Decoder EntityInstance
decoder =
    D.succeed EntityInstance
        |> DP.required "__grid" (D.list D.int)
        |> DP.required "__identifier" D.string
        |> DP.required "__pivot" (D.list D.float)
        |> DP.required "__tile" (D.nullable tileDecoder)
        |> DP.required "defUid" D.int
        |> DP.required "fieldInstances" (D.list FieldInstance.decoder)
        |> DP.required "height" D.int
        |> DP.required "px" (D.list D.int)
        |> DP.required "width" D.int


tileDecoder : D.Decoder Tile
tileDecoder =
    D.map2 Tile
        (D.field "srcRect" <| D.list D.int)
        (D.field "tilesetUid" D.int)
