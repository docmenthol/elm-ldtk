module LDTK.Enum exposing (Enum, decoder, decoderAlt)

import Json.Decode as D
import Json.Decode.Pipeline as DP


type alias Enum =
    { externalRelPath : Maybe String
    , iconTilesetUid : Maybe Int
    , identifier : String
    , uid : Int
    , values : List EnumValue
    }


type alias EnumValue =
    { tileSrcRect : Maybe (List Int)
    , color : Int
    , id : String
    , tileId : Maybe Int
    }


decoder : D.Decoder Enum
decoder =
    D.succeed Enum
        |> DP.required "externalRelPath" (D.nullable D.string)
        |> DP.required "iconTilesetUid" (D.nullable D.int)
        |> DP.required "identifier" D.string
        |> DP.required "uid" D.int
        |> DP.required "values" (D.list valueDecoder)


decoderAlt : D.Decoder Enum
decoderAlt =
    D.succeed Enum
        |> DP.required "externalRelPath" (D.nullable D.string)
        |> DP.required "iconTilesetUid" (D.nullable D.int)
        |> DP.required "identifier" D.string
        |> DP.required "uid" D.int
        |> DP.required "values" (D.list valueDecoderAlt)


valueDecoder : D.Decoder EnumValue
valueDecoder =
    D.succeed EnumValue
        |> DP.required "__tileSrcRect" (D.nullable <| D.list D.int)
        |> DP.required "color" D.int
        |> DP.required "id" D.string
        |> DP.required "tileId" (D.nullable D.int)


valueDecoderAlt : D.Decoder EnumValue
valueDecoderAlt =
    D.map4 EnumValue
        (D.field "__tileSrcRect" <| D.nullable (D.list D.int))
        (D.field "color" D.int)
        (D.field "id" D.string)
        (D.field "tileId" <| D.nullable D.int)
