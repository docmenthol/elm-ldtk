module LDTK.Layout exposing (Layout(..), decoder)

import Json.Decode as D


type Layout
    = Free
    | GridVania
    | LinearHorizontal
    | LinearVertical


decoder : D.Decoder Layout
decoder =
    let
        toType layout =
            case layout of
                "GridVania" ->
                    D.succeed GridVania

                "LinearHorizontal" ->
                    D.succeed LinearHorizontal

                "LinearVertical" ->
                    D.succeed LinearVertical

                _ ->
                    D.succeed Free
    in
    D.string |> D.andThen toType
