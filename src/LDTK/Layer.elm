module LDTK.Layer exposing (Layer, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP


type alias Layer =
    { layerType : String
    , autoSourceLayerDefUid : Maybe Int
    , autoTilesetDefUid : Maybe Int
    , displayOpacity : Float
    , gridSize : Int
    , identifier : String
    , intGridValues : List GridValue
    , pxOffsetX : Int
    , pxOffsetY : Int
    , tilesetDefUid : Maybe Int
    , uid : Int
    }


type alias GridValue =
    { color : String
    , identifier : Maybe String
    , value : Int
    }


decoder : D.Decoder Layer
decoder =
    D.succeed Layer
        |> DP.required "__type" D.string
        |> DP.required "autoSourceLayerDefUid" (D.nullable D.int)
        |> DP.required "autoTilesetDefUid" (D.nullable D.int)
        |> DP.required "displayOpacity" D.float
        |> DP.required "gridSize" D.int
        |> DP.required "identifier" D.string
        |> DP.required "intGridValues" (D.list gridValueDecoder)
        |> DP.required "pxOffsetX" D.int
        |> DP.required "pxOffsetY" D.int
        |> DP.required "tilesetDefUid" (D.nullable D.int)
        |> DP.required "uid" D.int


gridValueDecoder : D.Decoder GridValue
gridValueDecoder =
    D.succeed GridValue
        |> DP.required "color" D.string
        |> DP.required "identifier" (D.nullable D.string)
        |> DP.required "value" D.int
