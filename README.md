# sli/elm-ldtk

An LDTK loader for Elm. Currently decodes only the game-oriented JSON data. Mo editor-oriented data is decoded.

### Supported LDTK versions

* `0.9.*`

### TODO

* Some backwards compatibility.

### Notes

* https://ldtk.io/json/
