module LDTK.Entity exposing (Entity, decoder)

import Json.Decode as D
import Json.Decode.Pipeline as DP


type alias Entity =
    { color : String
    , height : Int
    , identifier : String
    , pivotX : Float
    , pivotY : Float
    , tileId : Maybe Int
    , tilesetId : Maybe Int
    , uid : Int
    , width : Int
    }


decoder : D.Decoder Entity
decoder =
    D.succeed Entity
        |> DP.required "color" D.string
        |> DP.required "height" D.int
        |> DP.required "identifier" D.string
        |> DP.required "pivotX" D.float
        |> DP.required "pivotY" D.float
        |> DP.required "tileId" (D.nullable D.int)
        |> DP.required "tilesetId" (D.nullable D.int)
        |> DP.required "uid" D.int
        |> DP.required "width" D.int
