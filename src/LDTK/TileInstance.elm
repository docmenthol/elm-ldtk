module LDTK.TileInstance exposing (..)

import Json.Decode as D


type alias TileInstance =
    { f : Int
    , px : List Int
    , arc : List Int
    , t : Int
    }


decoder : D.Decoder TileInstance
decoder =
    D.map4 TileInstance
        (D.field "f" D.int)
        (D.field "px" <| D.list D.int)
        (D.field "arc" <| D.list D.int)
        (D.field "t" D.int)
